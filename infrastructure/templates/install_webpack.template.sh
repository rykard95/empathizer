#!/usr/bin/env bash
GIT_REPO=ouae
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
. ~/.nvm/nvm.sh

nvm install 6.4.0

npm install -g webpack webpack-dev-server
git clone $GIT_REPO

cd $GIT_REPO
npm install --save-dev babel-loader@7 babel-core babel-preset-env

#npm install babel-loader @babel/core @babel/preset-env webpack webpack-dev-server webpack-cli --save-dev
#webpack-dev-server --mode development --host 0.0.0.0 --port 3000 --public empathizer-1254878570.us-west-2.elb.amazonaws.com
