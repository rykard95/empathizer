var assert = require("assert");
var sinon = require('sinon');

import {
    getFaceCount,
    getEmotions,
    getStudentGaze,
    getEngagementSCore,
    getStudentIsPresent
} from "../src/empathizer.mjs"

describe('EmpathizerInterface', function() {
    describe('getFaceCount()', function() {
        it('should return 1', function() {
            assert.equal([1,2,3].indexOf(4), -1);
        });
    });
});