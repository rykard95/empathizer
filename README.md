# empathizer.js

## Motivation
Intelligent tutoring systems currently do not leverage any
human metrics when deciding what content and what feedback
to present to the student. `empathizer.js` hopes to change
that.

## Current API
| Function | Description | Return Type |
|--------------|-------------|-------------|
|`getFaceCount`|Returns the number of faces detected from the webcam.|`integer`|
|`getEmotion`|Returns the emotion that is detected as a probability table.|`map (string => float)`|
|`getStudentGaze`|Returns the coordinates of the page that the student is gazing at.|`map (string => integer)`|
|`getEngagementScore (Planned)`|Returns a score that measures how engaged a student is, will be machine learning driven. Will be beta.|`float`|
|`getStudentIsPresent`|Returns a boolean value indicating if a student is in front of the camera or not.|`boolean`|

## Demo
https://www.empathizer-demo.com

## Installation
For now, clone this repo

## Dependencies
[Webgazer.js](https://webgazer.cs.brown.edu/) - external library used to export gaze coordinates. 

## Example Usage


