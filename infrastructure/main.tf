terraform {
  backend "s3" {
    bucket = "051164277233-dev-infra-uw2"
    key    = "terraform.tfstate"
    region = "us-west-2"
  }
}

locals {
  vpc_id = "vpc-5fb76027"
  keypair = "empathizer-server"
}

resource "aws_security_group" "server" {
  name        = "allow_all"
  description = "Allow all inbound traffic"
  vpc_id      = "${local.vpc_id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags {
    Name = "Allow all traffic"
  }
}





