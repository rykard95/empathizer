const config = {
    entry: ['./src/index.mjs'],
    output: {
        path: __dirname + '/build',
        filename: 'index.js'
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    resolve: {
        extensions: ['.js']
    },
    devServer: {
        port: 3000,
        contentBase: __dirname + '/build',
        inline: true
    }
};
module.exports = config;