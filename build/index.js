/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/empathizer.mjs":
/*!****************************!*\
  !*** ./src/empathizer.mjs ***!
  \****************************/
/*! exports provided: addTextToBody, getFaceCount, getEmotions, getStudentGaze, getEngagementSCore, getStudentIsPresent */
/***/ (function(__webpack_module__, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"addTextToBody\", function() { return addTextToBody; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getFaceCount\", function() { return getFaceCount; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getEmotions\", function() { return getEmotions; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getStudentGaze\", function() { return getStudentGaze; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getEngagementSCore\", function() { return getEngagementSCore; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getStudentIsPresent\", function() { return getStudentIsPresent; });\nfunction addTextToBody(text) {\n  var div = document.createElement('div');\n  div.textContent = text;\n  document.body.appendChild(div);\n}\nfunction getFaceCount() {\n  return 2;\n}\nfunction getEmotions() {\n  return {\n    \"happy\": 0.2,\n    \"sad\": 0.1,\n    \"angry\": 0.7\n  };\n}\nfunction getStudentGaze() {\n  return {\n    \"x\": 2,\n    \"y\": 3\n  };\n}\nfunction getEngagementSCore() {\n  return 0.4;\n}\nfunction getStudentIsPresent() {\n  return true;\n}\n\nfunction getWebCam() {\n  var constraints = {\n    video: true\n  };\n  var videoElement = document.getElementById('video'); // var video = document.getElementById('video');\n  // if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {\n  //     navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {\n  //         video.src = window.URL.createObjectURL(stream);\n  //         video.play();\n  //     });\n  // }\n\n  return true;\n}\n\nfunction getStream() {\n  if (window.stream) {\n    window.stream.getTracks().forEach(function (track) {\n      track.stop();\n    });\n  }\n\n  var constraints = {\n    video: {\n      deviceId: {\n        exact: videoSelect.value\n      }\n    }\n  };\n  navigator.mediaDevices.getUserMedia(constraints).then(gotStream).catch(handleError);\n}\n\nfunction gotStream(stream) {\n  window.stream = stream; // make stream available to console\n}\n\nfunction handleError(error) {\n  console.error('Error: ', error);\n}\n\nfunction hasGetUserMedia() {\n  return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);\n}\n\n//# sourceURL=webpack:///./src/empathizer.mjs?");

/***/ }),

/***/ "./src/index.mjs":
/*!***********************!*\
  !*** ./src/index.mjs ***!
  \***********************/
/*! no exports provided */
/***/ (function(__webpack_module__, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _empathizer_mjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./empathizer.mjs */ \"./src/empathizer.mjs\");\n\nvar faceCount = Object(_empathizer_mjs__WEBPACK_IMPORTED_MODULE_0__[\"getFaceCount\"])();\nvar student_gaze = Object(_empathizer_mjs__WEBPACK_IMPORTED_MODULE_0__[\"getStudentGaze\"])();\nvar emotions = Object(_empathizer_mjs__WEBPACK_IMPORTED_MODULE_0__[\"getEmotions\"])();\nvar engagement_score = Object(_empathizer_mjs__WEBPACK_IMPORTED_MODULE_0__[\"getEngagementSCore\"])();\nvar student_is_present = Object(_empathizer_mjs__WEBPACK_IMPORTED_MODULE_0__[\"getStudentIsPresent\"])();\nObject(_empathizer_mjs__WEBPACK_IMPORTED_MODULE_0__[\"addTextToBody\"])(\"I see \".concat(faceCount, \" faces.\"));\nObject(_empathizer_mjs__WEBPACK_IMPORTED_MODULE_0__[\"addTextToBody\"])(\"The student is gazing at \".concat(student_gaze.x, \", \").concat(student_gaze.y, \".\"));\nObject(_empathizer_mjs__WEBPACK_IMPORTED_MODULE_0__[\"addTextToBody\"])(\"The student emotion is \".concat(emotions.happy, \" happy, \").concat(emotions.sad, \" sad, \").concat(emotions.angry, \" angry.\"));\nObject(_empathizer_mjs__WEBPACK_IMPORTED_MODULE_0__[\"addTextToBody\"])(\"The engagement score is \".concat(engagement_score, \".\"));\nObject(_empathizer_mjs__WEBPACK_IMPORTED_MODULE_0__[\"addTextToBody\"])(\"The student is present: \".concat(student_is_present, \".\"));\n\n//# sourceURL=webpack:///./src/index.mjs?");

/***/ }),

/***/ 0:
/*!*****************************!*\
  !*** multi ./src/index.mjs ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__(/*! ./src/index.mjs */\"./src/index.mjs\");\n\n\n//# sourceURL=webpack:///multi_./src/index.mjs?");

/***/ })

/******/ });