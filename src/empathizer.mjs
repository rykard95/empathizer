let dataStore = {};

export function getFaceCount() {
    return dataStore["numFaces"];
}

export function getEmotions() {
    return dataStore["emotion"];
}

function startGazeTracker() {
    webgazer.setRegression('weightedRidge') /* currently must set regression and tracker */
        .setTracker('clmtrackr')
        .begin()
        .showPredictionPoints(false);
}

export function getStudentGaze() {
    let data = webgazer.getCurrentPrediction();
    if (data == null) {
        return;
    }

    let gaze = {};
    gaze.x = data.x;
    gaze.y = data.y;
    return gaze;
}

export function getEngagementSCore() {
    return 0.4
}

export function getStudentIsPresent() {
    return getFaceCount() >= 1;
}
let numIterations = 0;
let mainLoop = function() {
    if (numIterations > 2) {
        window.clearInterval(loopVar);
        alert("Empathizer library stops sending emotion requests after 25 seconds to curb demo costs.")
    }
    numIterations += 1;
    getWebcamScreenshot(getCurrentEmotion);
};

AWS.config.region = 'us-west-2'; // Region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-west-2:e2941958-b25c-47d5-845a-e6deaf478162',
});
let s3 = new AWS.S3();

let videoElement = document.getElementById('video');
const canvas = document.createElement('canvas');

let getWebcamScreenshot = function(callback) {
    canvas.width = videoElement.videoWidth;
    canvas.height = videoElement.videoHeight;
    canvas.getContext('2d').drawImage(videoElement, 0, 0);
    // Other browsers will fall back to image/png
    canvas.toBlob(callback);
};

let getCurrentEmotion = function(blob) {
    putIntoS3Bucket(blob, getEmotionFromAzure);
};

let putIntoS3Bucket = function(blob, successCallback) {
    let randomName = Math.random().toString(36).substring(7);
    let image = new File([blob], `${randomName}.jpeg`);
    let params = {
        Body: image,
        Bucket: "051164277233-dev-data-uw2",
        Key: `${randomName}.jpeg`,
        ACL: "public-read"
    };
    s3.putObject(params, function(err) {
        if (err) {
            console.error(err, err.stack);
        }
        else {
            successCallback(randomName, emitEmotion, function() {deleteFromS3Bucket(randomName)});
        }
    });
    return randomName;
};

let deleteFromS3Bucket = function(imageName) {
    let params = {
        Bucket: "051164277233-dev-data-uw2",
        Key: `${imageName}.jpeg`
    };
    s3.deleteObject(params, function(err) {
        if (err) console.error(err, err.stack);
        else console.debug(`Deletion of ${imageName}.jpeg Successful!`);
    });
};

let getEmotionFromAzure = function(imageName, successCallback, errorCallback) {
    let url = "https://westus2.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceAttributes=emotion";
    let classifyRequestData = `{'url': 'https://s3-us-west-2.amazonaws.com/051164277233-dev-data-uw2/${imageName}.jpeg'}`;
    console.debug(classifyRequestData);
    $.ajax({
        url: url,
        type: "post",
        data: classifyRequestData,
        headers: {
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": "a3ab88374e2042b4992cf6f358ddf25c"
        },
        success: function(data) {
            successCallback(imageName, data);
        },
        error: function(data) {
            errorCallback(imageName, data);
        }
    });
};

let emitEmotion = function(imageName, data) {
    dataStore["emotion"] = data[0]["faceAttributes"]["emotion"];
    dataStore["numFaces"] = data.length;
    deleteFromS3Bucket(imageName);
};

startGazeTracker();
let loopVar = window.setInterval(mainLoop, 5000);
