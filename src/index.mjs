import {
    getFaceCount,
    getEmotions,
    getStudentGaze,
    getEngagementSCore,
    getStudentIsPresent
} from "./empathizer.mjs"

let mainLoop = function() {
    updateMetricAggregate();
    updateMetricEmotion();
};

let updateMetricGaze = function() {
    let metricGaze = document.getElementById("metrics-gaze");
    let gaze = webgazer.getCurrentPrediction();
    metricGaze.innerText =
        `Current gaze at (${gaze.x}, ${gaze.y})
        If the current gaze prediction isn't good, keep clicking on the moving blue pixel.`
};

let updateMetricAggregate = function() {
    let metricAggregate = document.getElementById("metrics-aggregate");
    metricAggregate.innerText =
        `Additional metrics\n
         numberOfFaces: ${getFaceCount()}
         studentIsPresent: ${getStudentIsPresent()}
         engagementScore: ${1.0}
        `
};

let updateMetricEmotion = function() {
    let metricEmotion = document.getElementById("metrics-emotion");
    let emotions = getEmotions();
    metricEmotion.innerHTML =
        `Currently displayed emotions <br><br>
         <div style="display: inline-block; width: 100px;">Happy: ${emotions.happiness}</div> Sad: ${emotions.sadness} <br> 
         <div style="display: inline-block; width: 100px;"> Angry: ${emotions.anger}</div> Contempt: ${emotions.contempt} <br>
         <div style="display: inline-block; width: 100px;"> Neutral: ${emotions.neutral}</div> Suprise: ${emotions.surprise} <br>
         <div style="display: inline-block; width: 100px;"> Disgust: ${emotions.disgust}</div>
        `
};

let nextMap = {
    "first-tip": "second-tip",
    "second-tip": "third-tip",
    "third-tip": "fourth-tip",
    "fourth-tip": null
};

let isCalibrated = false;

let setupGazeImprover = function() {
    let gazeImprover = document.getElementById("gaze-improving-pixel");
    gazeImprover.style.top = "500px";
    gazeImprover.style.left = "350px";
    gazeImprover.onclick = function() {
        let newTop = Math.round(Math.random() * window.innerHeight);
        let newLeft = Math.round(Math.random() * window.innerWidth);
        gazeImprover.style.top = `${newTop}px`;
        gazeImprover.style.left = `${newLeft}px`;
    };
};
setupGazeImprover();

let updateCalibrationClicks = function() {
   $(".calibration-click-button").click(function(e) {
       let colorList = ["red", "blue", "green", "orange"];
       let target = e.target;
       let timesClicked = e.target.attributes.timesclicked;
       let colorIndex = parseInt(timesClicked.value);
       console.log(`clicked! ${timesClicked.value}`);
       if (timesClicked.value >= 2) {
            colorIndex = 2;
            let nextTip = nextMap[target.parentElement.parentElement.id];
            target.parentElement.parentElement.style.display = "none";

            if (nextTip != null) {
                let nextTipBox = document.getElementById(nextTip);
                nextTipBox.style.display = "initial";
            } else {
                isCalibrated = true;
            }
       }
       target.style.backgroundColor = colorList[colorIndex];
       timesClicked.value++;
   })
};
updateCalibrationClicks();

let drawGazeCoordinates = function() {
    if (!isCalibrated) {
        console.log("GazeTracker uncalibrated.");
        return null;
    }
    let data = webgazer.getCurrentPrediction();
    console.log(data);
    let gazePixel = document.getElementById("gaze-pixel");
    gazePixel.style.top = `${data.y}px`;
    gazePixel.style.left = `${data.x}px`;

    updateMetricGaze();
};

let startWebGazer = function() {
    webgazer.setRegression('weightedRidge') /* currently must set regression and tracker */
    .setTracker('clmtrackr')
    .begin()
    .showPredictionPoints(false);
};

startWebGazer();
setInterval(drawGazeCoordinates, 200);
setInterval(mainLoop, 1000);

// setTimeout(function() {
//     setInterval(function() {
//         mainLoop();
//     }, 5000)
// }, 2000);
